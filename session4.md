# Research seminar on probabilistic data analysis: Session 4 (May 1, 2020)

  * [Zoom link](zoom.md)
  * [Zulip chat](https://utu.zulipchat.com/#narrow/stream/230589-bda)


# Discussion topic 1 (Guilhem Sommeria-Klein): Hierarchical models

[BDA Book Chapter 5](http://www.stat.columbia.edu/~gelman/book/)

Main reading:

 The chapter is fairly long, so I propose to:
 * focus on sections 5.1-3;
 * go through section 5.4 more quickly;
 * look at [the corresponding demo](http://avehtari.github.io/BDA_R_demos/demos_ch5/demo5_1.html);
 * and look at exercises 5.1-2 and 5.4-5 at the end of the chapter (section 5.9).
 
Optionally, you can also:

 * read the rest of the chapter (sections 5.5-7);
 * check the [slides from A. Vehtari](https://github.com/avehtari/BDA_course_Aalto/blob/master/slides/slides_ch5.pdf).

# Discussion topic 2 (Paul-Christian Burkner): brms R package

[Presentation slides](Bayesian_Multilevel_Modeling_brms_Paul_Buerkner.pdf)

Main reading covers the two main brms papers:

 * [brms: An R Package for Bayesian Multilevel Models Using Stan](https://www.jstatsoft.org/article/view/v080i01)
 * [Advanced Bayesian Multilevel Modeling with the R Package brms](https://journal.r-project.org/archive/2018/RJ-2018-017/index.html)
 



