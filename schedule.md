# Probabilistic data analysis: research seminar 2020

## Schedule

The following links list study material for each session (available one week in advance for each session).

[Session 1](session1.md) (March 20, 2020)

  * Introduction (Chapters 1-2; Leo Lahti)  
     
[Session 2](session2.md) (April 3, 2020)

  * Priors (Chapters 2-3; Ville Laitinen)
  * rstanarm R package (Related to chapters 1-3; Leo Lahti)  
     
[Session 3](session3.md) (April 17, 2020 / Aaro Salosensaari & Felix Vaura)

  * Posterior approximations (Chapter 4.1-4.4; Aaro Salosensaari)
  * Relation to other methods (Chapter 4.5+; Felix Vaura)  
     
[Session 4](session4.md) (May 1, 2020)

  * Hierarchical models (Chapter 5; Guilhem Sommeria-Klein)
  * BRMS R package (Related to chapter 5; Paul-Christian Burkner)  
     
[Session 5](session5.md) (May 15, 2020)

  * Model checking & feature selection (Chapters 6-7; David Kohns)
  * Model comparison (Chapter 7; Raoul Wolf)       

[Session 6](session6.md) (May 29, 2020)

  * Stan (Extra material; Dmytro Perepolkin)
  * Applications of Stan/BDA (TBA; Iiro Tiihonen)

[Session 7](session7.md) (June 12, 2020)

  * Decision analysis (Chapter 9; Joonatan Palmu)       
  * Approximations & sampling (Chapter 11; Rohit Goswami)      

[Session 8](session8.md) (June 26, 2020)

  * Bayesian computation (VI / HMC?) (Chapters 10 & 12; Moein Khalighi)
  * Discussion and conclusion (Leo Lahti)

## Participation

Each participant will:

  1. study the material and do the exercises in advance before every session
  1. participate actively in the sessions, and
  1. in his/her turn choose and announce study material one week before the next session

For other practical information, see [README](README.md) (time, location, contact details).

## Source material

To prepare a session on your own turn, check the following [material](material.md).

