# notes for BDA session 3, ch 4.1-4.4

Aaro Salosensaari

## Homework was:

* read chapter 4.1-4.5
* read demo 4.1. Also check example 3.7.
* those who are curious about more variational inference, look at Chapter 13, but I won't cover that. 

## Chapter 4, Sections 4.1-4.5

* Look at Section 4.1

* Look at demo 4.1 here.

* If someone wants to do theoretical excercises, I will also cover ex 4.4


## Section 4.1

* Approximate posterior with normal distribution.
* Justified by analysis of Taylor series expansion of $log p(\theta \, | \, y)$ at $\hat{\theta}$, $n$ large.
  * First term of expansion is constant, second $\propto log$ posterior.

* Five points about uses of this.

1. Approximation useful itself.
2. Posterior density contours relative to posterior maximum. Amount of posterior mass within particular contour. Contours in two dimensions.
3. Summarizing posterior distributions by point estimates and standard errors. In some cases corresponds to frequentists intervals.
4. Data reduction and summary statistics. Similar to above, but in connection to later chapter 5.
5. Final point about lower-dimensional normal approximations: Normal approximations often better in low dimensional $\theta$. If theta is high dimensional, the normal approximation often better for low-dimensional marginals or conditional distributions than the full joint distributions.

## Demo 4.1: Normal approximation to the posterior distribution

```{r libraries}
#' ggplot2, grid, and gridExtra are used for plotting, tidyr for
#' manipulating data frames
#+ setup, message=FALSE, error=FALSE, warning=FALSE
library(ggplot2)
library(gridExtra)
library(tidyr)
library(MASS)
```

### Description

Bioassay experiment. BDA3 section 3.7. p. 74.

In experiment, we treat $n=5$ animals each with a drug/chemical at $k = 4$ different dose levels $x$, given in log scale (log g/ml), to estimate at which dose the drug becomes poisonous.


```{r data}
#' Bioassay data, (BDA3 page 86)
df1 <- data.frame(
  x = c(-0.86, -0.30, -0.05, 0.73),
  n = c(5, 5, 5, 5),
  y = c(0, 1, 3, 5)
)
```

### Grid sampling of posterior

This should be familiar now so we don't look at this with too much detail.

In brief, our model is logistic regression

$$y_i | \theta_i \sim \mathrm{Bin}(n_i, \theta_i)$$

$$\mathrm{logit}(\theta_i) = \alpha + \beta x_i.$$

Additional notes.

* Prior is uniform on some parameter range.

* Note that $\mathrm{logit}(\theta) = \log(\theta / 1-\theta)$ and (logistic) $\mathrm{logit}^{-1}(z) = \exp(z) / (\exp(z) + 1)$.

* Likelihood is $p(y_i \, | \, \alpha, beta, n_i, x_i) \propto \mathrm{logit}^{-1}(\alpha + \beta x_i)^{y_i} (1 - \mathrm{logit}^{-1} (\alpha + \beta x_i))^{n_i - y_i}$.

* thus log likelihood is

$y_i \left((\alpha + \beta x_i) - \log(\exp(\alpha + \beta x_i) + 1) \right) + (n_i - y_i) \log (1/(\exp(\alpha + \beta x_i) +1))$ 

$= y_i (\alpha + \beta x_i) - y_i \log(\exp(\alpha + \beta x_i) + 1) + y_i \log (\exp(\alpha + \beta x_i) +1) - n_i  \log (\exp(\alpha + \beta x_i) +1)$

$= y_i (\alpha + \beta x_i) - n_i  \log (\exp(\alpha + \beta x_i) +1)$.


* LD50 is the dose level at which probability of death is 0.50. See p. 77.

### First we sample the exact posterior on a grid

```{r grid_posterior}
#' ### Grid sampling for Bioassay model.

#' Compute the posterior density in a grid
#' 
#' - usually should be computed in logarithms!
#' - with alternative prior, check that range and spacing of A and B
#'   are sensible
A = seq(-1.5, 7, length.out = 100)
B = seq(-5, 35, length.out = 100)
# make vectors that contain all pairwise combinations of A and B
cA <- rep(A, each = length(B))
cB <- rep(B, length(A))

# a helper function to calculate the log likelihood
logl <- function(df, a, b)
  df['y']*(a + b*df['x']) - df['n']*log1p(exp(a + b*df['x']))

# calculate likelihoods: apply logl function for each observation
# ie. each row of data frame of x, n and y
p <- apply(df1, 1, logl, cA, cB) %>% rowSums() %>% exp()

#' Sample from the grid (with replacement)
nsamp <- 1000
samp_indices <- sample(length(p), size = nsamp,
                       replace = T, prob = p/sum(p))
samp_A <- cA[samp_indices[1:nsamp]]
samp_B <- cB[samp_indices[1:nsamp]]
# add random jitter, see BDA3 p. 76
samp_A <- samp_A + runif(nsamp, A[1] - A[2], A[2] - A[1])
samp_B <- samp_B + runif(nsamp, B[1] - B[2], B[2] - B[1])

#' Compute LD50 conditional beta > 0
bpi <- samp_B > 0
samp_ld50 <- -samp_A[bpi]/samp_B[bpi]
```

### Let us plot it

```{r grid_plots1}
#' Create a plot of the posterior density
# limits for the plots
xl <- c(-1.5, 7)
yl <- c(-5, 35)
pos <- ggplot(data = data.frame(cA ,cB, p), aes(x = cA, y = cB)) +
  geom_raster(aes(fill = p, alpha = p), interpolate = T) +
  geom_contour(aes(z = p), colour = 'black', size = 0.2) +
  coord_cartesian(xlim = xl, ylim = yl) +
  labs(x = 'alpha', y = 'beta') +
  scale_fill_gradient(low = 'yellow', high = 'red', guide = F) +
  scale_alpha(range = c(0, 1), guide = F)

pos
```
```{r grid_plots2}
#' Plot of the samples
sam <- ggplot(data = data.frame(samp_A, samp_B)) +
  geom_point(aes(samp_A, samp_B), color = 'blue', size = 0.3) +
  coord_cartesian(xlim = xl, ylim = yl) +
  labs(x = 'alpha', y = 'beta')

sam
```
```{r grid_plots3}
#' Plot of the histogram of LD50
his <- ggplot() +
  geom_histogram(aes(samp_ld50), binwidth = 0.04,
                 fill = 'steelblue', color = 'black') +
  coord_cartesian(xlim = c(-0.8, 0.8)) +
  labs(x = 'LD50 = -alpha/beta')

his

# log g /ml
```

### Next we do normal approximation for the posterior

Idea: approximate posterior of $\theta = (\alpha, \beta)$ by a multi-variate Normal distribution at posterior mode, with covariance equal to curvature of posterior.

At large $n$ and with parameter values $\theta$, asymptotic theory says approximation is good.

1. Numerical optimization of negative log likelihood to obtain posterior mode $\hat{\theta}$. (Same as MLE because of uniform prior.)
2. Numerical optimization also yields a matrix of second derivatives (Hessian) of n.l.l. or observed information $I(\theta)$.
3. Approximate posterior by Normal $N(\hat{\theta}, I(\theta)^{-1})$.


### Optimiation

```{r normal1a}
#' ### Normal approximation for Bioassay model.

#' Define the negative log likelihood function to be optimized
bioassayfun <- function(w, df) {
  z <- w[1] + w[2]*df$x # alpha + beta x_i
  # -sum_i y_i*(alpha + beta x_i) - n_i * log(exp(alpha + beta x_i) + 1)
  -sum(df$y*(z) - df$n*log1p(exp(z)))
}

#' Optimize. We will find parameter values w (= alpha, beta), otpim also returns hessian.
w0 <- c(0,0)
optim_res <- optim(w0, bioassayfun, gr = NULL, df1, hessian = T)
w <- optim_res$par
S <- solve(optim_res$hessian)
```

### Multivariate normal

```{r normal1b}
#' Multivariate normal probability density function
dmvnorm <- function(x, mu, sig)
  exp(-0.5*(length(x)*log(2*pi) + log(det(sig)) + (x-mu)%*%solve(sig, x-mu)))


#' Evaluate likelihood at points (cA,cB) 
#' this is just for illustration and would not be needed otherwise
p <- apply(cbind(cA, cB), 1, dmvnorm, w, S)

# sample from the multivariate normal 
normsamp <- mvrnorm(nsamp, w, S)

```

### Derived quantities (LD50)

```{r normal2}
#' Samples of LD50 conditional beta > 0:
#' Normal approximation does not take into account that the posterior
#' is not symmetric and that there is very low density for negative
#' beta values. Based on the draws from the normal approximation
#' is is estimated that there is about 5% probability that beta is negative!
bpi <- normsamp[,2] > 0
normsamp_ld50 <- -normsamp[bpi,1]/normsamp[bpi,2]
```

### Plots

```{r normal_plots}
#' Create a plot of the posterior density
pos_norm <- ggplot(data = data.frame(cA ,cB, p), aes(x = cA, y = cB)) +
  geom_raster(aes(fill = p, alpha = p), interpolate = T) +
  geom_contour(aes(z = p), colour = 'black', size = 0.2) +
  coord_cartesian(xlim = xl, ylim = yl) +
  labs(x = 'alpha', y = 'beta') +
  scale_fill_gradient(low = 'yellow', high = 'red', guide = F) +
  scale_alpha(range = c(0, 1), guide = F)

#' Plot of the samples
sam_norm <- ggplot(data = data.frame(samp_A=normsamp[,1], samp_B=normsamp[,2])) +
  geom_point(aes(samp_A, samp_B), color = 'blue', size = 0.3) +
  coord_cartesian(xlim = xl, ylim = yl) +
  labs(x = 'alpha', y = 'beta')

#' Plot of the histogram of LD50
his_norm <- ggplot() +
  geom_histogram(aes(normsamp_ld50), binwidth = 0.04,
                 fill = 'steelblue', color = 'black') +
  coord_cartesian(xlim = c(-0.8, 0.8)) +
  labs(x = 'LD50 = -alpha/beta, beta > 0')

#' Combine the plots
grid.arrange(pos, sam, his, pos_norm, sam_norm, his_norm, ncol = 3)
```

### Additional points, p. 85


### 4.2. Review of asymptotic theory

Proofs at appendix. B, maybe look at them if time.


### 4.3. Counterexamples to theorems


### 4.4. Frequency evaluations of Bayesian inferences




