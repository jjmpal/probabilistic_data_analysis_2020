data {
  int N; // number of groups
  int M; // samples size/group
  matrix[M, N] Y;
}

parameters {
  real mu[N];
  real<lower=0> sigma;
  
  // hyperparameters
  real mu_mean;
  real<lower=0> mu_sd;
  
}

model {
  for(i in 1:N) {
    Y[, i] ~ normal(mu[i], sigma);
  }
  
  mu ~ normal(mu_mean, mu_sd);
  
}

generated quantities {
  real Y6_tilda = normal_rng(mu[6], sigma);
  
  real new_mu = normal_rng(mu_mean, mu_sd);
  real Y7_tilda = normal_rng(new_mu, sigma);
}