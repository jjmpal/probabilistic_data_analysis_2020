data {
  int N;
  vector[N] x;
  vector[N] y;
  real xpred;
}

parameters {
  real alpha; 
  real beta;
  real<lower=0> sigma;
}

model {
  y ~ normal(alpha + beta*x, sigma);
  
  // Prior
  beta ~ normal(0, 27);
}

generated quantities {
  real y_pred = normal_rng(alpha + beta*xpred, sigma);
}