data {
  int M;
  vector[M] Y;
}

parameters {
  real mu;
  real<lower=0> sigma;
}

model {
  Y ~ normal(mu, sigma);
}

generated quantities {
  real Y_tilda = normal_rng(mu, sigma);
}