# Research seminar on probabilistic data analysis: Session 5 (May 15, 2020)

  * [Zoom link](zoom.md)
  * [Zulip chat](https://utu.zulipchat.com/#narrow/stream/230589-bda)



# Discussion topic 1 (Dmytro Perepolkin): Intro to Stan

Watch the intro by Ben Lambert: [How to write your first Stan
program](https://www.youtube.com/watch?v=YZZSYIx1-mw).

The files that Ben produced in this video can be found
[here](https://github.com/dmi3kno/StanTutorial).

Check more on Stan in the [Reference
Manual](https://mc-stan.org/docs/2_23/reference-manual/index.html)

Optional:

 * We will be talking a little bit about Posterior Predictive Checks,
   so you can check out [Chapter 26 of the User
   Guide](https://mc-stan.org/docs/2_23/stan-users-guide/ppcs-chapter.html)


# Discussion topic 2 (Iiro Tiihonen): Demo
* The demo is about applying Stan to fitting Gaussian Process models
* For the purposes of this demo, a good introduction to Gaussian Process modeling is given by https://royalsocietypublishing.org/doi/full/10.1098/rsta.2011.0550
* For fitting such models with Stan, please read the related chapter 10 of the official Stan guide https://mc-stan.org/docs/2_23/stan-users-guide/gaussian-processes-chapter.html
* I might come up with additional demonstrative material later, but understanding the core ideas presented in the introduction text and the Stan implementation of the guide is enough.