# Research seminar on probabilistic data analysis: Session 5 (May 15, 2020)

  * [Zoom link](zoom.md)
  * [Zulip chat](https://utu.zulipchat.com/#narrow/stream/230589-bda)



# Discussion topic 1 (Joonatan Palmu): Chapter 9; decision analysis


# Discussion topic 2 (Rohit Goswami): Chapter 11; approximations and sampling
